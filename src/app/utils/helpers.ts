export const SimulateAPI = <T>(time: number = 1000, data: T) => {
  return new Promise<T>((resolve) => {
    setTimeout(() => {
      resolve(data);
    }, time);
  });
};
