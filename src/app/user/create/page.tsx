import React, { useState } from "react";
import { revalidatePath } from "next/cache";
import { SimulateAPI } from "@/app/utils/helpers";
import { redirect } from "next/navigation";

const UserPage = () => {
  async function createInvoice(data: FormData) {
    "use server";
    const dataToSave = {
      id: Math.random(),
      name: data.get("name"),
    };

    const response = await SimulateAPI(2000, dataToSave);

    redirect(`/user/${response.id}`);
    // mutate data
  }

  return (
    <div className="p-4">
      <form action={createInvoice}>
        <div className="flex flex-col mb-4">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            id="name"
            name="name"
            className="p-2 bg-neutral-300 text-black"
          />
        </div>
        <div className="flex flex-col mb-4">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            id="email"
            name="email"
            className="p-2 bg-neutral-300 text-black"
          />
        </div>
        <button type="submit" className="p-2 bg-neutral-300 text-black">
          Create User
        </button>
      </form>
    </div>
  );
};

export default UserPage;
