import { Metadata } from "next";
import React, { PropsWithChildren } from "react";
export const metadata: Metadata = {
  title: "User Create",
  description: "User create page",
};
const UserPageLayout: React.FC<PropsWithChildren> = (props) => {
  return <>{props.children}</>;
};

export default UserPageLayout;
