"use client";
import React, { useState } from "react";

const UserPage = () => {
  const [counterVal, setCountVal] = useState(0);
  return (
    <div className="p-4">
      <button
        className="p-2 bg-neutral-300 text-black"
        onClick={() => {
          setCountVal(counterVal + 1);
        }}
      >
        Increase
      </button>
      <button
        className="p-2 bg-neutral-300 text-black"
        onClick={() => {
          setCountVal(counterVal - 1);
        }}
      >
        Decrease
      </button>
      <p>Counter Value: {counterVal}</p>
    </div>
  );
};

export default UserPage;
