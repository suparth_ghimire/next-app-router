import { Metadata } from "next";
import React, { PropsWithChildren } from "react";
export const metadata: Metadata = {
  title: "This is title",
  description: "This is description",
};
const UserPageLayout: React.FC<PropsWithChildren> = (props) => {
  return <>{props.children}</>;
};

export default UserPageLayout;
