import { Metadata } from "next";
import React, { PropsWithChildren } from "react";

// export const metadata: Metadata = {
//   title: "This is Single User Page of id ?",
//   description: "This is description",
// };

type T_Props = {
  params: {
    id: string;
  };
};

const delay = (id: string) =>
  new Promise<{ name: string }>((res) =>
    setTimeout(
      () =>
        res({
          name: `John Doe wioth id ${id}`,
        }),
      1000
    )
  );

export async function generateMetadata({ params }: T_Props) {
  const user = await delay(params.id);
  return {
    title: `${user.name} | Apop Name`,
  };
}

const UserPageLayout: React.FC<PropsWithChildren> = (props) => {
  return <>{props.children}</>;
};

export default UserPageLayout;
