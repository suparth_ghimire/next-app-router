"use client";
import React from "react";

type T_Props = {
  error: Error;
  reset: () => void;
};

const UserErrorPage: React.FC<T_Props> = (props) => {
  return <div>ERROR: {props.error.message}</div>;
};

export default UserErrorPage;
