import { SimulateAPI } from "@/app/utils/helpers";
import React from "react";
type T_Props = {
  params: {
    id: string;
  };
};

type T_User = {
  ok: true;
  name: string;
};

type T_Error = {
  ok: false;
  message: string;
};

const UserPage: React.FC<T_Props> = async (props) => {
  const user = await SimulateAPI<T_Error | T_User>(1000, {
    ok: true,
    name: "John Doe",
  });

  if (!user.ok) throw new Error(user.message);

  return <div>User Fetched: {user.name}</div>;
};

export default UserPage;
